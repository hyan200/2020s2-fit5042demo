package fit5042.ass.repository.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import fit5042.ass.repository.entities.Customer;

/**
 *
 * @author Hongli
 */
@Entity
@Table(name = "Customer_Contact")
@NamedQueries({
    @NamedQuery(name = CustomerContact.GET_ALL_QUERY_NAME, query = "SELECT c FROM CustomerContact c")})

public class CustomerContact implements Serializable {

    public static final String GET_ALL_QUERY_NAME = "CustomerContact.getAll";

    private int customerContactId;
    private String title;
    private String homePhone;
    private String workPhone;
    private String email;
    
    private Customer customer;

    public CustomerContact() {
    }

   

    public CustomerContact(int customerContactId, String title, String homePhone, String workPhone, String email,
			Customer customer) {
		super();
		this.customerContactId = customerContactId;
		this.title = title;
		this.homePhone = homePhone;
		this.workPhone = workPhone;
		this.email = email;
		this.customer = customer;
	}



	@Id
    @GeneratedValue
    @Column(name = "customer_contact_id")
	public int getCustomerContactId() {
		return customerContactId;
	}



	public void setCustomerContactId(int customerContactId) {
		this.customerContactId = customerContactId;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getHomePhone() {
		return homePhone;
	}



	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}



	public String getWorkPhone() {
		return workPhone;
	}



	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}


	@ManyToOne
	public Customer getCustomer() {
		return customer;
	}



	public void setCustomer(Customer customer) {
		this.customer = customer;
	}


}
    