package fit5042.ass.repository.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import fit5042.ass.repository.entities.Address;
import fit5042.ass.repository.entities.CustomerContact;

/**
 *
 * @author Hongli
 */
@Entity
@NamedQueries({
    @NamedQuery(name = Customer.GET_ALL_QUERY_NAME, query = "SELECT c FROM Customer c order by c.customerId desc")})


public class Customer implements Serializable {
	public static final String GET_ALL_QUERY_NAME = "Customer.getAll";


    private int customerId;
    private String name;
    private String gender;
    private Double boughtPrice;
    private String industryType;
    private int age;

    private Address address;
    
    private Set<CustomerContact> customerContacts;


	public Customer(int customerId, String name, String gender, Double boughtPrice, String industryType, int age,
			Address address, Set<CustomerContact> customerContacts) {
		super();
		this.customerId = customerId;
		this.name = name;
		this.gender = gender;
		this.boughtPrice = boughtPrice;
		this.industryType = industryType;
		this.age = age;
		this.address = address;
		this.customerContacts = customerContacts;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "customer_id")
	public int getCustomerId() {
		return customerId;
	}


	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Double getBoughtPrice() {
		return boughtPrice;
	}

	public void setBoughtPrice(Double boughtPrice) {
		this.boughtPrice = boughtPrice;
	}

	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Embedded
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
    
	@OneToMany(mappedBy = "customer")
	public Set<CustomerContact> getCustomerContacts() {
		return customerContacts;
	}

	public void setCustomerContacts(Set<CustomerContact> customerContacts) {
		this.customerContacts = customerContacts;
	}

	public Customer() {
        this.customerContacts = new HashSet<>();
    }
}
