package fit5042.ass.repository;

import java.util.List;
import java.util.Set;

import javax.ejb.Remote;

import fit5042.ass.repository.entities.Customer;
import fit5042.ass.repository.entities.CustomerContact;

@Remote 

public interface CustomerRepository {
	
	
	
	public void addCustomer(Customer customer) throws Exception;

    
    public Customer searchCustomerById(int id) throws Exception;

    
    public List<Customer> getAllCustomers() throws Exception;
  
     
    public void removeCustomer(int csutomerId) throws Exception;

    public List<CustomerContact> getAllCustomerContact() throws Exception ;
    
    public void editCustomer(Customer customer) throws Exception;

   
   
}

	
