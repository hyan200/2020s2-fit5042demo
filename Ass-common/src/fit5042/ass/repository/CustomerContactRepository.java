package fit5042.ass.repository;

import java.util.List;

import javax.ejb.Remote;


import fit5042.ass.repository.entities.CustomerContact;

@Remote
public interface CustomerContactRepository {
    public void addCustomerContact(CustomerContact customerContact) throws Exception ;
    
    public CustomerContact searchCustomerContactById(int id) throws Exception;
    
    public List<CustomerContact> getAllCustomerContacts() throws Exception ;

    public void removeCustomerContact(int customerContactId) throws Exception;
    
    public void editCustomerContact(CustomerContact customerContact) throws Exception ;
}
