package fit5042.ass.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fit5042.ass.repository.entities.Customer;
import fit5042.ass.repository.entities.CustomerContact;

@Stateless
public class CustomerRepositorySessionBean implements CustomerRepository {

	@PersistenceContext(unitName = "Ass-ejb")//insert code (annotation) here to use container managed entity manager to complete these methods  
    private EntityManager entityManager;

    @Override
    public void addCustomer(Customer customer) throws Exception {
        List<Customer> customers = entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
        customer.setCustomerId(customers.get(0).getCustomerId() + 1);
        entityManager.persist(customer);
    }

    @Override
    public Customer searchCustomerById(int id) throws Exception {
    	Customer customer = entityManager.find(Customer.class, id);
    	//customer.getTags();
        return customer;
    }

    @Override
    public List<Customer> getAllCustomers() throws Exception {
        return entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
    }
    

    @Override
    public List<CustomerContact> getAllCustomerContact() throws Exception {
    	return entityManager.createNamedQuery(CustomerContact.GET_ALL_QUERY_NAME).getResultList();
    }


    @Override
    public void removeCustomer(int customerId) throws Exception {
        //complete this method
    	Customer customer = this.searchCustomerById(customerId);

        if (customer != null) {
            entityManager.remove(customer);
        }
    }

    @Override
    public void editCustomer(Customer customer) throws Exception {
        try {
            entityManager.merge(customer);
        } catch (Exception ex) {

        }
    }

    
}
