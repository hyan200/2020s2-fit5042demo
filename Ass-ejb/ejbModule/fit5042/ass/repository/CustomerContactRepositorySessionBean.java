package fit5042.ass.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import javax.ejb.Stateless;

import fit5042.ass.repository.entities.CustomerContact;

@Stateless
public class CustomerContactRepositorySessionBean  implements CustomerContactRepository {
	@PersistenceContext(unitName = "Ass-ejb")//insert code (annotation) here to use container managed entity manager to complete these methods  
    private EntityManager entityManager;

    @Override
    public void addCustomerContact(CustomerContact customerContact) throws Exception {
        List<CustomerContact> customerContacts = entityManager.createNamedQuery(CustomerContact.GET_ALL_QUERY_NAME).getResultList();
        customerContact.setCustomerContactId(customerContacts.get(0).getCustomerContactId() + 1);
        entityManager.persist(customerContact);
    }

    @Override
    public CustomerContact searchCustomerContactById(int id) throws Exception {
    	CustomerContact customerContact = entityManager.find(CustomerContact.class, id);
    	//customer.getTags();
        return customerContact;
    }

    @Override
    public List<CustomerContact> getAllCustomerContacts() throws Exception {
        return entityManager.createNamedQuery(CustomerContact.GET_ALL_QUERY_NAME).getResultList();
    }
    
/***
    @Override
   
   // public List<Customer> getAllCustomer() throws Exception {
   //     return entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
    }
*/
    @Override
    public void removeCustomerContact(int customerContactId) throws Exception {
        //complete this method
    	CustomerContact customerContact = this.searchCustomerContactById(customerContactId);

        if (customerContact != null) {
            entityManager.remove(customerContact);
        }
    }

    @Override
    public void editCustomerContact(CustomerContact customerContact) throws Exception {
        try {
            entityManager.merge(customerContact);
        } catch (Exception ex) {

        }
    }
}
