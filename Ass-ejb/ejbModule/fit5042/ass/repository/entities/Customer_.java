package fit5042.ass.repository.entities;

import java.util.Set;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2020-09-01T00:01:17.009+0800")
@StaticMetamodel(Customer.class)
public class Customer_ {
	public static volatile SingularAttribute<Customer, Integer> customerId;
	public static volatile SingularAttribute<Customer, Address> address;
	public static volatile SingularAttribute<Customer, String> companyName;
	public static volatile SingularAttribute<Customer, Integer> employeeNumber;
	
	public static volatile SingularAttribute<Customer, Double> boughtPrice;
	public static volatile SingularAttribute<Customer, String> industryType;
	public static volatile SingularAttribute<Customer, Double> annualSales;
	public static volatile SetAttribute<Customer, CustomerContact> customerContacts;

}

