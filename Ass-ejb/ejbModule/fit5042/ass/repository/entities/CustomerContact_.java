package fit5042.ass.repository.entities;
import java.util.Set;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2020-09-01T00:01:17.009+0800")
@StaticMetamodel(CustomerContact.class)
public class CustomerContact_ {
	public static volatile SingularAttribute<Customer, Integer> customerContactId;
	public static volatile SingularAttribute<Customer, String> name;
	public static volatile SingularAttribute<Customer, String> phoneNumber;
	public static volatile SingularAttribute<Customer, Customer> customer;

}