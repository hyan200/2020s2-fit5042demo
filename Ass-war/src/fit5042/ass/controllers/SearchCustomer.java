package fit5042.ass.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;


import fit5042.ass.controllers.Customer;
import fit5042.ass.controllers.CustomerApplication;

@RequestScoped
@Named("searchCustomer")
public class SearchCustomer {
private Customer customer;
    
    CustomerApplication app;
    private int searchByInt;
    
    public CustomerApplication getApp() {
        return app;
    }

    public void setApp(CustomerApplication app) {
        this.app = app;
    }
     
    public void setCustomer(Customer customer){
        this.customer = customer;
    }
    public int getSearchByInt() {
        return searchByInt;
    }

    public void setSearchByInt(int searchByInt) {
        this.searchByInt = searchByInt;
    }
    
    public Customer getCustomer(){
        return customer;
    }
    
    public SearchCustomer() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (CustomerApplication) FacesContext.getCurrentInstance()
                        .getApplication()
                        .getELResolver()
                        .getValue(context, null, "customerApplication");
        
        app.updateCustomerList();
    }

    /**
     * Normally each page should have a backing bean but you can actually do it
     * any how you want.
     *
     * @param property Id 
     */
    public void searchCustomerById(int customerId) 
    {
       try
       {
            //search this property then refresh the list in PropertyApplication bean
            app.searchCustomerById(customerId);
       }
       catch (Exception ex)
       {
           
       }
    }
    
   
    
    public void searchAll() 
    {
       try
       {
            //return all properties from db via EJB
             app.searchAll();
       }
       catch (Exception ex)
       {
           
       }
    }
}
