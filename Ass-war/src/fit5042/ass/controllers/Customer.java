package fit5042.ass.controllers;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import fit5042.ass.repository.entities.Address;
import fit5042.ass.repository.entities.CustomerContact;

/**
 *
 * @author Hongli
 */
@RequestScoped
@Named(value = "customer")

public class Customer implements Serializable {
	


    private int customerId;
    private String name;
    private String gender;
    private Double boughtPrice;
    private String industryType;
    private int age;
    
    private Address address;
    
    private String streetNumber;
    private String streetAddress;
    private String suburb;
    private String postcode;
    private Set<fit5042.ass.repository.entities.Customer> Customers;

    private Set<fit5042.ass.repository.entities.CustomerContact> CustomerContacts;
    
    
	
    
    public Set<fit5042.ass.repository.entities.Customer> getCustomers() {
		return Customers;
	}

	public void setCustomers(Set<fit5042.ass.repository.entities.Customer> customers) {
		Customers = customers;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getSuburb() {
		return suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	private String state;
    
    private Set<CustomerContact> customerContacts;


	public Customer(int customerId, String name, String gender, Double boughtPrice, String industryType, int age,
			Address address, Set<CustomerContact> customerContacts) {
		super();
		this.customerId = customerId;
		this.name = name;
		this.gender = gender;
		this.boughtPrice = boughtPrice;
		this.industryType = industryType;
		this.age = age;
		this.address = address;
		this.customerContacts = customerContacts;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "customer_id")
	public int getCustomerId() {
		return customerId;
	}


	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Double getBoughtPrice() {
		return boughtPrice;
	}

	public void setBoughtPrice(Double boughtPrice) {
		this.boughtPrice = boughtPrice;
	}

	public String getIndustryType() {
		return industryType;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}


	public Set<CustomerContact> getCustomerContacts() {
		return customerContacts;
	}

	public void setCustomerContacts(Set<CustomerContact> customerContacts) {
		this.customerContacts = customerContacts;
	}

	public Customer() {
        this.customerContacts = new HashSet<>();
    }
}
