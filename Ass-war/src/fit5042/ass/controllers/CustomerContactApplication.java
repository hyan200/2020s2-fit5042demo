package fit5042.ass.controllers;

import java.util.ArrayList;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.ass.mbeans.CustomerContactManagedBean;
import fit5042.ass.repository.entities.CustomerContact;

/**
 * The class is a demonstration of how the application scope works. You can
 * change this scope.
 *
 * @author hongli yang
 */
@Named(value = "customerContactApplication")
@ApplicationScoped

public class CustomerContactApplication {

    //dependency injection of managed bean here so that we can use its methods
    @ManagedProperty(value = "#{customerContactManagedBean}")
    CustomerContactManagedBean customerContactManagedBean;

    private ArrayList<CustomerContact> customerContacts;

    private boolean showForm = true;

    public boolean isShowForm() {
        return showForm;
    }

    // Add some property data from db to app 
    public CustomerContactApplication() throws Exception {
    	customerContacts = new ArrayList<>();

        //instantiate propertyManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerContactManagedBean = (CustomerContactManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerContactManagedBean");

        //get properties from db 
        updateCustomerContactList();
    }

    public ArrayList<CustomerContact> getCustomerContacts() {
        return customerContacts;
    }

    private void setCustomerContacts(ArrayList<CustomerContact> newCustomerContacts) {
        this.customerContacts = newCustomerContacts;
    }

    //when loading, and after adding or deleting, the property list needs to be refreshed
    //this method is for that purpose
    public void updateCustomerContactList() {
        if (customerContacts != null && customerContacts.size() > 0)
        {
            
        }
        else
        {
        	customerContacts.clear();

            for (fit5042.ass.repository.entities.CustomerContact customerContact : customerContactManagedBean.getAllCustomerContacts())
            {
            	customerContacts.add(customerContact);
            }

            setCustomerContacts(customerContacts);
        }
    }

    public void searchCustomerContactById(int customerContactId) {
    	customerContacts.clear();

    	customerContacts.add(customerContactManagedBean.searchCustomerContactById(customerContactId));
    }

        
    public void searchAll()
    {
    	customerContacts.clear();
        
        for (fit5042.ass.repository.entities.CustomerContact customerContact : customerContactManagedBean.getAllCustomerContacts())
        {
        	customerContacts.add(customerContact);
        }
        
        setCustomerContacts(customerContacts);
    }
}
