package fit5042.ass.controllers;
import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.ass.mbeans.CustomerContactManagedBean;
import javax.faces.bean.ManagedProperty;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hongli yang
 */
@RequestScoped
@Named("removeCustomerContact")
public class RemoveCustomerContact {

    @ManagedProperty(value = "#{customerContactManagedBean}")
    CustomerContactManagedBean customerContactManagedBean;

    private boolean showForm = true;

    
    private CustomerContact customerContact;

    CustomerContactApplication app;

    public void setCustomerContact(CustomerContact customerContact) {
        this.customerContact = customerContact;
    }

    public CustomerContact getCustomerContact() {
        return customerContact;
    }

    public boolean isShowForm() {
        return showForm;
    }

    public RemoveCustomerContact() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (CustomerContactApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerContactApplication");

        app.updateCustomerContactList();

        //instantiate CustomerContactManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerContactManagedBean = (CustomerContactManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerContactManagedBean");
    }

    /**
     * @param CustomerContact Id
     */
    public void removeCustomerContact(int customerContactId) {
        try {
            //remove this CustomerContact from db via EJB
            customerContactManagedBean.removeCustomerContact(customerContactId);

            //refresh the list in CustomerContactApplication bean
             app.searchAll();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("CustomerContact succesfully deleted"));
        } catch (Exception ex) {

        }
        showForm = true;

    }

}

