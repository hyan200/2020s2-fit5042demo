package fit5042.ass.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;


@Named(value = "customerController")
@RequestScoped
public class CustomerController {
	private int customerId; //this propertyId is the index, don't confuse with the Property Id
    
    private fit5042.ass.repository.entities.Customer customer;
    
    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
    

    public CustomerController() {
        // Assign property identifier via GET param 
        //this propertyID is the index, don't confuse with the Property Id
        customerId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("customerID"));
        // Assign property based on the id provided 
        customer = getCustomer();
    }

    public fit5042.ass.repository.entities.Customer getCustomer() {
        if (customer == null) {
            // Get application context bean PropertyApplication 
            ELContext context
                    = FacesContext.getCurrentInstance().getELContext();
            CustomerApplication app
                    = (CustomerApplication) FacesContext.getCurrentInstance()
                            .getApplication()
                            .getELResolver()
                            .getValue(context, null, "customerApplication");
            // -1 to propertyId since we +1 in JSF (to always have positive property id!) 
            return app.getCustomers().get(--customerId); //this propertyId is the index, don't confuse with the Property Id
        }
        return customer;
    }
}
