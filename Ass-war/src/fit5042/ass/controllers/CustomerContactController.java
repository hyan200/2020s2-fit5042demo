package fit5042.ass.controllers;

import javax.el.ELContext;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author hongli yang
 */
@Named(value = "customerContactController")
@RequestScoped
public class CustomerContactController {

    private int customerContactId; //this CustomerContactId is the index, don't confuse with the CustomerContact Id

    public int getCustomerContactId() {
        return customerContactId;
    }

    public void setCustomerContactId(int customerContactId) {
        this.customerContactId = customerContactId;
    }
    private fit5042.ass.repository.entities.CustomerContact customerContact;

    public CustomerContactController() {
        // Assign CustomerContact identifier via GET param 
        //this CustomerContactID is the index, don't confuse with the CustomerContact Id
        customerContactId = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("customerContactID"));
        // Assign CustomerContact based on the id provided 
        customerContact = getCustomerContact();
    }

    public fit5042.ass.repository.entities.CustomerContact getCustomerContact() {
        if (customerContact == null) {
            // Get application context bean CustomerContactApplication 
            ELContext context
                    = FacesContext.getCurrentInstance().getELContext();
            CustomerContactApplication app
                    = (CustomerContactApplication) FacesContext.getCurrentInstance()
                            .getApplication()
                            .getELResolver()
                            .getValue(context, null, "customerContactApplication");
            // -1 to CustomerContactId since we +1 in JSF (to always have positive CustomerContact id!) 
            return app.getCustomerContacts().get(--customerContactId); //this CustomerContactId is the index, don't confuse with the CustomerContact Id
        }
        return customerContact;
    }
}
