package fit5042.ass.controllers;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;


@Named(value = "titleController")
@RequestScoped
public class TitleController {

    private String pageTitle;

    public TitleController() {
        
        pageTitle = "Australian Printings Pty Ltd";
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }
}