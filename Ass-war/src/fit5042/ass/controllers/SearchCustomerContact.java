package fit5042.ass.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hongli Yang
 */
@RequestScoped
@Named("searchCustomerContact")
public class SearchCustomerContact {
    private boolean showForm = true;

    private CustomerContact customerContact;

    CustomerContactApplication app;

    private int searchByInt;

    public CustomerContactApplication getApp() {
        return app;
    }

    public void setApp(CustomerContactApplication app) {
        this.app = app;
    }


    public int getSearchByInt() {
        return searchByInt;
    }

    public void setSearchByInt(int searchByInt) {
        this.searchByInt = searchByInt;
    }


    public void setCustomerContact(CustomerContact customerContact) {
        this.customerContact = customerContact;
    }

    public CustomerContact getCustomerContact() {
        return customerContact;
    }

    public boolean isShowForm() {
        return showForm;
    }

    public SearchCustomerContact() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (CustomerContactApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerContactApplication");

        app.updateCustomerContactList();

    }

    /**
     * Normally each page should have a backing bean but you can actually do it
     * any how you want.
     *
     * @param CustomerContact Id
     */
    public void searchCustomerContactById(int customerContactId) {
        try {
            //search this CustomerContact then refresh the list in CustomerContactApplication bean
            app.searchCustomerContactById(customerContactId);
        } catch (Exception ex) {

        }
        showForm = true;

    }


    public void searchAll() {
        try {
            //return all properties from db via EJB
            app.searchAll();
        } catch (Exception ex) {

        }
        showForm = true;
    }

}