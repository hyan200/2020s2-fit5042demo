package fit5042.ass.controllers;
import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.ass.mbeans.CustomerContactManagedBean;

import javax.faces.bean.ManagedProperty;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hongli yang
 */
@RequestScoped
@Named("addCustomerContact")
public class AddCustomerContact {

    @ManagedProperty(value = "#{customerContactManagedBean}")
    CustomerContactManagedBean customerContactManagedBean;

    private boolean showForm = true;

    private CustomerContact customerContact;

    CustomerContactApplication app;

    public void setCustomerContact(CustomerContact customerContact) {
        this.customerContact = customerContact;
    }

    public CustomerContact getCustomerContact() {
        return customerContact;
    }

    public boolean isShowForm() {
        return showForm;
    }

    public AddCustomerContact() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (CustomerContactApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "customerContactApplication");

        //instantiate propertyManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerContactManagedBean = (CustomerContactManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerContactManagedBean");
    }

    public void addCustomerContact(CustomerContact localCustomerContact) {
        //this is the local property, not the entity
        try {
            //add this property to db via EJB
        	customerContactManagedBean.addCustomerContact(localCustomerContact);

            //refresh the list in PropertyApplication bean
             app.searchAll();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("CustomerContact has been added succesfully"));
        } catch (Exception ex) {

        }
        showForm = true;
    }

}
