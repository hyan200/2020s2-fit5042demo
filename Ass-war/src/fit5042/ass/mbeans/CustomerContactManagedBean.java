package fit5042.ass.mbeans;
import fit5042.ass.repository.CustomerContactRepository;
import fit5042.ass.repository.entities.Address;
import fit5042.ass.repository.entities.Customer;
import fit5042.ass.repository.entities.CustomerContact;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author hongli yang
 */
@ManagedBean(name = "customerContactManagedBean")
@SessionScoped

public class CustomerContactManagedBean implements Serializable {

    @EJB
    CustomerContactRepository customerContactRepository;

    /**
     * Creates a new instance of CustomerContactManagedBean
     */
    public CustomerContactManagedBean() {
    }

    public List<CustomerContact> getAllCustomerContacts() {
        try {
            List<CustomerContact> customerContacts = customerContactRepository.getAllCustomerContacts();
            return customerContacts;
        } catch (Exception ex) {
            Logger.getLogger(CustomerContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void addCustomerContact(CustomerContact customerContact) {
        try {
        	customerContactRepository.addCustomerContact(customerContact);
        } catch (Exception ex) {
            Logger.getLogger(CustomerContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Search a CustomerContact by Id
     */
    public CustomerContact searchCustomerContactById(int id) {
        try {
            return customerContactRepository.searchCustomerContactById(id);
        } catch (Exception ex) {
            Logger.getLogger(CustomerContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }


    public void removeCustomerContact(int customerContactId) {
        try {
        	customerContactRepository.removeCustomerContact(customerContactId);
        } catch (Exception ex) {
            Logger.getLogger(CustomerContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editCustomerContact(CustomerContact customerContact) {
        try {
            int s = customerContact.getCustomerContactId();
            customerContact.setCustomerContactId(s);
            String b = customerContact.getTitle();
            customerContact.setTitle(b);
            String c = customerContact.getHomePhone();
            customerContact.setHomePhone(c);
            String d = customerContact.getWorkPhone();
            customerContact.setWorkPhone(d);
            String e = customerContact.getEmail();
            customerContact.setEmail(e);

            customerContactRepository.editCustomerContact(customerContact);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("CustomerContact has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(CustomerContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }



    public void addCustomerContact(fit5042.ass.controllers.CustomerContact localCustomerContact) {
        //convert this newCustomerContact which is the local CustomerContact to entity CustomerContact
    	CustomerContact CustomerContact = convertCustomerContactToEntity(localCustomerContact);

        try {
        	customerContactRepository.addCustomerContact(CustomerContact);
        } catch (Exception ex) {
            Logger.getLogger(CustomerContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private CustomerContact convertCustomerContactToEntity(fit5042.ass.controllers.CustomerContact localCustomerContact) {
    	CustomerContact customerContact = new CustomerContact(); 
        String title = localCustomerContact.getTitle();
        String homePhone = localCustomerContact.getHomePhone();
        String workPhone = localCustomerContact.getWorkPhone();
        String email = localCustomerContact.getEmail();
        customerContact.setEmail(localCustomerContact.getEmail());
        customerContact.setWorkPhone(localCustomerContact.getHomePhone());
        customerContact.setTitle(localCustomerContact.getTitle());
        customerContact.setHomePhone(localCustomerContact.getHomePhone());
        customerContact.setCustomer(localCustomerContact.getCustomer());
        return customerContact;
    }

}
