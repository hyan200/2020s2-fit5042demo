package fit5042.ass.mbeans;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import fit5042.ass.repository.CustomerRepository;
import fit5042.ass.repository.entities.Address;
import fit5042.ass.repository.entities.Customer;
import fit5042.ass.repository.entities.CustomerContact;


@ManagedBean(name = "customerManagedBean")
@SessionScoped
public class CustomerManagedBean implements Serializable {

    @EJB
    CustomerRepository customerRepository;

    /**
     * Creates a new instance of CustomerManagedBean
     */
    public CustomerManagedBean() {
    }

    public List<Customer> getAllCustomers() {
        try {
            List<Customer> customers = customerRepository.getAllCustomers();
            return customers;
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void addCustomer(Customer customer) {
        try {
        	customerRepository.addCustomer(customer);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Search a Customer by Id
     */
    public Customer searchCustomerById(int id) {
        try {
            return customerRepository.searchCustomerById(id);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    

    public List<CustomerContact> getAllCustomerContact() throws Exception {
        try {
            return customerRepository.getAllCustomerContact();
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public void removeCustomer(int customerId) {
        try {
        	customerRepository.removeCustomer(customerId);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editCustomer(Customer customer) {
        try {
            String s = customer.getAddress().getStreetNumber();
            Address address = customer.getAddress();
            address.setStreetNumber(s);
            customer.setAddress(address);

            customerRepository.editCustomer(customer);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }



    public void addCustomer(fit5042.ass.controllers.Customer localCustomer) {
        //convert this newCustomer which is the local Customer to entity Customer
    	Customer customer = convertCustomerToEntity(localCustomer);

        try {
        	customerRepository.addCustomer(customer);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Customer convertCustomerToEntity(fit5042.ass.controllers.Customer localCustomer) {
    	Customer customer = new Customer(); //entity
        String streetNumber = localCustomer.getStreetNumber();
        String streetAddress = localCustomer.getStreetAddress();
        String suburb = localCustomer.getSuburb();
        String postcode = localCustomer.getPostcode();
        String state = localCustomer.getState();
        Address address = new Address(streetNumber, streetAddress, suburb, postcode, state);
        customer.setAddress(address);
        customer.setName(localCustomer.getName());
        customer.setGender(localCustomer.getGender());
        customer.setBoughtPrice(localCustomer.getBoughtPrice());
        customer.setIndustryType(localCustomer.getIndustryType());
        customer.setAge(localCustomer.getAge());
        customer.setCustomerContacts(localCustomer.getCustomerContacts());
        return customer;
    }

}