package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Junyang
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() {
    	try {
            this.propertyRepository.addProperty(new Property(1, "31 birdwood st", 2, 150, 400));
            this.propertyRepository.addProperty(new Property(2, "23 woodfull court", 3, 352, 800));
            this.propertyRepository.addProperty(new Property(3, "12 box st", 5, 800, 600));
            this.propertyRepository.addProperty(new Property(4, "3 hbhvb St", 2, 170, 200));
            this.propertyRepository.addProperty(new Property(5, "432 hill st", 1, 60, 700));
            System.out.println("5 properties added successfully");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
        
    
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() {
    	try {
    		for(Property property : propertyRepository.getAllProperties()) {
        		System.out.println(property);
        	}
    	} catch (Exception e) {
			System.out.println(e.getMessage());
		}
        
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() {
    	Scanner sc = new Scanner(System.in);
        System.out.print("Please enter the property ID ");
        try {
            int input = sc.nextInt();
            Property property = this.propertyRepository.searchPropertyById(input);
            if (input == 0 || input == 1 || input == 2 || input == 3 || input == 4 || input == 5) {
                
                System.out.println(property.toString());
            } else {
                System.out.println("Property does not exist.");
            }
        } catch (Exception e) {
            System.out.println("Please input an Integer: " + e.getMessage());
        }
    }
    
    public void run() {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }
}
