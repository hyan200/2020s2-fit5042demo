/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Junyang
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository{
	private ArrayList<Property> properties = new ArrayList<Property>();

    public SimplePropertyRepositoryImpl() {
    	this.properties = new ArrayList<>();
    }
    
    public void addProperty(Property property){
    	properties.add(property);
    }
    
    
    public Property searchPropertyById(int id){
    	int a = 0;
    	int index = 0;
    	try {
    		while ( a < properties.size()){
        		if(properties.get(a).getId() == id) {
        			
        			index = a;
        			break;
        		}
        		else {
        			a++;
        		}
        	}
    	} catch (Exception e) {
			
    		e.printStackTrace();
		}
    	return properties.get(index);
    }
    
    public List<Property> getAllProperties(){
    	return properties;
    }
}
